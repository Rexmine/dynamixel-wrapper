from abc import ABC, abstractmethod
from .dynamixel_sdk import COMM_SUCCESS, PacketHandler, PortHandler
from typing import Union

DEFAULT_ERROR_MESSAGE = "Erreur sur le Dynamixel ID: {id}\n{error_message}\n\nOriginal Error:\n{original_error}"
NOT_CONNECTED_ERROR = "La connexion au Dynamixel n'a pas été établie."


class DynamixelError(Exception):
    pass


class Dynamixel(ABC):
    protocole_version = 1.0

    def __init__(self, port_name: str, baud_rate: int):
        self.baud_rate = baud_rate
        self.port_name = port_name

        self.connected = False
        self.debug = False
        self.locked = False
        self.id_locked = 0

        self.initialize()

    def initialize(self):
        self.port_handler = PortHandler(self.port_name)
        self.packet_handler = PacketHandler(self.protocole_version)

        self.min_position: int
        self.max_position: int

        self.addr_torque_enable: int
        self.addr_goal_position: int
        self.addr_set_speed: int
        self.addr_present_position: int

        self.set_model_values()

    def ping(self, id: int):
        """
        Tente d'envoyer un ping sur ce Dynamixel.

        Raises:
            DynamixelError: Si cette méthode est appellée alors que le Dynamixel n'est pas connecté.

        Returns:
            bool: True si le ping a réussi, False sinon.
        """

        self.lock(id)

        if not self.connected:
            raise DynamixelError(NOT_CONNECTED_ERROR)
        
        _, comm_result, error = self.packet_handler.ping(self.port_handler, id)
        self.unlock(id)
        return self.get_error(id, comm_result, error)

    def enable_torque(self, id):
        """
        Active le torque sur ce Dynamixel.

        Raises:
            DynamixelError: Si cette méthode est appellée alors que le Dynamixel n'est pas connecté.
        """

        self.lock(id)

        if not self.connected:
            raise DynamixelError(NOT_CONNECTED_ERROR)
        
        comm_result, error = self.packet_handler.write1ByteTxRx(
            self.port_handler,
            id,
            self.addr_torque_enable,
            1
        )

        self.raise_error(id, comm_result, error, f"Echec lors de l'activation du torque.")
        self.unlock(id)

    def set_goal_position(self, id: int, position: int):
        """
        Indique la position cible du moteur.

        Args:
            position (int): L'angle souhaité, en degrés.

        Raises:
            DynamixelError: Si cette méthode est appellée alors que le Dynamixel n'est pas connecté.
        """
        
        self.lock(id)

        if not self.connected:
            raise DynamixelError(NOT_CONNECTED_ERROR)
        
        comm_result, error = self.packet_handler.write2ByteTxRx(
            self.port_handler,
            id,
            self.addr_goal_position,
            position
        )

        self.raise_error(id, comm_result, error, f"Echec lors de la mise de la position cible.")
        self.unlock(id)

    def get_present_position(self, id: int) -> int:
        """
        Récupère la position actuelle du Dynamixel

        Raises:
            DynamixelError: Si cette méthode est appellée alors que le Dynamixel n'est pas connecté.

        Returns:
            int: L'angle actuel du Dynamixel, en degrés.
        """

        self.lock(id)

        if not self.connected:
            raise DynamixelError(NOT_CONNECTED_ERROR)
        
        present_position, comm_result, error = self.packet_handler.read2ByteTxRx(
            self.port_handler,
            id,
            self.addr_present_position
        )

        self.get_error(id, comm_result, error)
        self.unlock(id)
        return present_position
    
    def go_to(self, id: int, position: int, precision: int = 20):
        """
        Tourne le moteur à une position, et attends que le mouvement soit terminé.

        Args:
            position (int): La position cible
            precision (int, optional): La précision du mouvement. Defaults to 20.

        Raises:
            DynamixelError: Si cette méthode est appellée alors que le Dynamixel n'est pas connecté.
        """

        self.lock(id)

        if not self.connected:
            raise DynamixelError(NOT_CONNECTED_ERROR)
        
        self.set_goal_position(id, position)
        while abs(self.get_present_position(id) - position) > precision:
            pass

        self.unlock(id)

    def set_speed(self, id: int, speed: int):
        self.lock(id)

        if not self.connected:
            raise DynamixelError(NOT_CONNECTED_ERROR)
        
        comm_result, error = self.packet_handler.write2ByteTxRx(
            self.port_handler,
            id,
            self.addr_set_speed,
            speed
        )

        self.get_error(id, comm_result, error)
        self.unlock(id)

    def get_error(self, id: int, comm_result, error, error_message: Union[str, None] = None, succes_message: Union[str, None] = None):
        if comm_result != COMM_SUCCESS:
            original_error = self.packet_handler.getTxRxResult(comm_result)
        elif error != 0:
            original_error = self.packet_handler.getRxPacketError(error)
        elif succes_message is not None:
            print(succes_message)
            return True
        else:
            return True
        
        if error_message is not None:
            print(DEFAULT_ERROR_MESSAGE.format(
                id=id,
                error_message=error_message,
                original_error=original_error
            ))

        return False

    def raise_error(self, id: int, comm_result, error, error_message: str):
        if comm_result != COMM_SUCCESS:
            original_error = self.packet_handler.getTxRxResult(comm_result)
        elif error != 0:
            original_error = self.packet_handler.getRxPacketError(error)
        else:
            return
        
        raise DynamixelError(DEFAULT_ERROR_MESSAGE.format(
            id=id,
            error_message=error_message,
            original_error=original_error
        ))
    
    def connect(self):
        result = self.port_handler.openPort()
        if result:
            if self.debug:
                print(f"Port {self.port_name} ouvert.")

        else:
            raise DynamixelError(f"Echec d'ouverture du port {self.port_name}.")
        
        result = self.port_handler.setBaudRate(self.baud_rate)
        if not result:
            raise DynamixelError(f"Echec lors du changement de baud rate.")

        self.connected = True

    def disconnect(self):
        if self.debug:
            print(f"Port {self.port_name} fermé.")

        self.connected = False
        self.port_handler.closePort()

    def lock(self, id: int):
        while self.locked and self.id_locked != id:
            pass

        self.locked = True
        self.id_locked = id
        if self.debug:
            print(f"Lock {id}")

    def unlock(self, id: int):
        while self.id_locked != id and self.locked == False:
            pass

        if self.debug:
            print(f"Unlock {id}")

        self.locked = False

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.disconnect()

    def with_debug(self):
        self.debug = True
        return self

    @abstractmethod
    def set_model_values(self):
        pass
