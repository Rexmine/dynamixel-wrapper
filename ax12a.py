from .dynamixel import Dynamixel

class AX12A(Dynamixel):
    def __init__(self, port_name: str, baud_rate: int = 1000000):
        super().__init__(port_name, baud_rate)

    def set_model_values(self):
        self.min_position = 0
        self.max_position = 1023

        self.addr_torque_enable = 24
        self.addr_goal_position = 30
        self.addr_set_speed = 32
        self.addr_present_position = 36
